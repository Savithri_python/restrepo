from __future__ import unicode_literals

from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
# Create your models here.

class Snippet(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	title = models.CharField(max_length=50, blank=True, default='')
	code = models.TextField()
	linenos = models.BooleanField(default=False)
	owner = models.ForeignKey('auth.User', related_name='snippets', on_delete = models.CASCADE)
	highlighted = models.TextField(default='')
	
	class Meta:
		ordering = ('created',)

	def __str__(self):
		return self.owner