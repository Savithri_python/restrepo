from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from django.views.generic.base import TemplateView
from . import views

urlpatterns = [
	url(r'angular/$', TemplateView.as_view(template_name='quickstart/get_json.html'), name='angular_url'),
	url(r'^snippets/$', views.snippet_list),
	url(r'^snippets/(?P<pk>[0-9]+)/$', views.snippet_details),
	url(r'^userlist/$', views.UserList.as_view(), name='user-list'),
	url(r'^userlist/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),
	url(r'^clssnippets/$', views.SnippetList.as_view(), name='snippet-list'),
	url(r'^clssnippets/(?P<pk>[0-9]+)/$', views.SnippetDetail.as_view(), name='snippet-detail'),
	url(r'^$', views.api_root),
	url(r'^snippets/(?P<pk>[0-9]+)/highlight/$', views.SnippetHighlight.as_view(), name='snippet-highlight'),
]

urlpatterns = format_suffix_patterns(urlpatterns)